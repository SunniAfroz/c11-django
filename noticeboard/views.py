from django.shortcuts import render, redirect
from .models import Notice
from .models import models
from .forms import NoticeForm

# Create your views here.

def notice_board(request):
    notices = Notice.objects.order_by('position')
    return render(request, 'noticeboard/notice_board.html', {'notices': notices})

def new_notice(request):
    if request.method == 'POST':
        form = NoticeForm(request.POST)
        if form.is_valid():
            if Notice.objects.count() < 6:
                position = Notice.objects.aggregate(models.Max('position'))['position__max']
                position = 0 if position is None else position + 1
            else:
                oldest_notice = Notice.objects.order_by('position').first()
                position = oldest_notice.position
                oldest_notice.delete()
            notice = form.save(commit=False)
            notice.position = position
            notice.save()

            return redirect('notice_board')
    else:
        form = NoticeForm()

    return render(request, 'noticeboard/new_notice.html', {'form': form})
