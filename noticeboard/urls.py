from django.urls import path
from . import views

urlpatterns = [
    path('notice_board', views.notice_board, name='notice_board'),
    path('new_notice', views.new_notice, name='new_notice'),
]