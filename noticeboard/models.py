from django.db import models

# Create your models here.

class Notice(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    contact_number = models.CharField(max_length=20)
    position = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.title